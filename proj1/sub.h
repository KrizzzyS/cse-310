//**************************************************************************************************************
// FILE: sub.h
//
// DESCRIPTION
// The subroutine header.
//
// AUTHOR INFORMATION
// Kristopher J. Smith
//
//
//
// Email: kristopher.j.smith@asu.edu
// Web:	  http://github.com/KristopherSmith
//**************************************************************************************************************
#ifndef SUB_H
#define SUB_H

int sub(int n, int *A, int *B, int *C);

#endif
