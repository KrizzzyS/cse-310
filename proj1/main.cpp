//**************************************************************************************************************
// FILE: Main.cpp
//
// DESCRIPTION
// The driver for project 1.
//
// AUTHOR INFORMATION
// Kristopher J. Smith
//
//
//
// Email: kristopher.j.smith@asu.edu
// Web:	  http://github.com/KristopherSmith
//**************************************************************************************************************
#include <iostream>
#include <fstream>
#include "sub.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;

int main() {
	int sizeOfArray;
	int *arrayA, *arrayB, *arrayC;
	ifstream inFile ("input.txt");
	if(inFile.is_open()) {
		inFile >> sizeOfArray;
		arrayA = new int[sizeOfArray];
		arrayB = new int[sizeOfArray];
		arrayC = new int[sizeOfArray];

		for(int i = 0; inFile.good(); i++) {
			int a, b, c;
			inFile >> a, inFile >> b, inFile >> c;
			arrayA[i] = a, arrayB[i] = b, arrayC[i] = c;
		}

		inFile.close();
	}
	else {
		cout << "input.txt could not be opened.";
		return -1;
	}

	ofstream outFile ("output.txt");
	if(outFile.is_open()) {
		outFile << sizeOfArray << endl;
		
		for(int i = 0; i < sizeOfArray; i++) {
			outFile << arrayA[i] << '\t';
			outFile << arrayB[i] << '\t';
			outFile << arrayC[i] << '\t';
			outFile << endl;
		}

		int result = sub(sizeOfArray, arrayA, arrayB, arrayC);
		outFile << result;
		outFile.close();
		delete[] arrayA;
		delete[] arrayB;
		delete[] arrayC;
	}
	else {
		cout << "output.txt could not be opened.";
		return -1;
	}

	return 0;
}
